//UVA 828


#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define     GSORT(x)          sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define     UNIQUE(v)         Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define     SF                scanf
#define     PF                printf

typedef long long ll;
typedef long double ld;

using namespace std;
vector<ll>vec[101];
ld mat[101][101] , d[101];
ld ans[101];
bool infp[101];
ll n ;

ll gs(){
    Rep(i ,n){
        if(fabs(mat[i][i])<EPS){
               bool as =false;
            For(j ,i+1, n){
                if(mat[j][i]>EPS){
                    as =true;
                    Rep(k , n+1) swap(mat[j][k] , mat[i][k]);
                }
            }
            if(!as )continue;
        }
        Rep(j , n){
            if(j != i){
                ld p = mat[j][i] / mat[i][i];
                Rep(k ,n+1) mat[j][k]-=(mat[i][k]*p);
            }
        }
    }
    return 0;
}
int main(){
//    Test;
//    Testout;
    ll cas =0;
    while(cin>>n){
        if(!n) break;
        Set(ans ,0);
        Set(infp, false);
        Set(mat ,0);
        Set(d ,0);
        ll x, y;
        Rep(i , n+1) vec[i].clear();
        while(cin>>x>>y){
            if(!x && !y) break;
            x-- , y--;
            vec[y].push_back(x);
            d[x]++;
        }
        mat[0][n]=1;
        Rep(i ,n)
        {
            mat[i][i]=1;
            for(auto j : vec[i]) mat[i][j] -= 1/d[j];
        }
//        Rep(i ,n){
//            Rep(j , n+1) cout<<mat[i][j]<<" ";
//            cout<<endl;
//        }
//            cout<<endl;
//            cout<<endl;
        gs();
//                Rep(i ,n){
//            Rep(j , n+1) cout<<mat[i][j]<<" ";
//            cout<<endl;
//        }
        for(int i = n-1; i>=0 ; i--){
            if(fabs(mat[i][i]) < EPS && fabs(mat[i][n])>EPS) infp[i]=true;
            else{
                if(fabs(mat[i][i])<EPS) ans[i]=0;
                 else ans[i] = mat[i][n]/mat[i][i];
                For(j ,i+1, n) if(fabs(mat[i][j])> EPS && infp[j] ==true) infp[i]=true;
            }
        }

            printf("Case #%lld:\n" , ++cas);
        ll q; cin >> q;
        Rep(i ,q){
            ll x ;cin >> x;
            if(infp[x-1]) printf("infinity\n");
            else printf("%0.3LF\n" , ans[x-1]);
        }
    }
}


// if 0 == 0  -> infinite solution   //for this problem node with infinite solution has 0 (say example)
//if 0 == number exept zero  ->no solution    // for this problem infinity is no solution this means there is 
//no solution for that node then that is infinity  