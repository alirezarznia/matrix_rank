//discribe this site: http://stattrek.com/matrix-algebra/matrix-rank.aspx
//test this site : http://www.math.odu.edu/~bogacki/cgi-bin/lat.cgi


//#include <GOD>
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
typedef long long ll;
using namespace std;
double mat[3][3];
int main(){
  //  Test;
    ll t;
    cin >> t;
    while(t--){
        Rep(i ,3){
            Rep(j,3) cin>>mat[i][j];
        }
        Rep(i ,3){
            if(mat[i][i]){
                    For(j ,i+1 ,3){
                        if(mat[j][i]){
                            double mm = (mat[j][i])/(mat[i][i]);
                            Rep(k ,3){
                                mat[j][k]-=(mm*mat[i][k]);
                            }
                        }
                    }
            }
            else{
                For(j ,i+1,3){
                    if(mat[j][i]){
                        Rep(k,3){
                            swap(mat[i][k] , mat[j][k]);
                        }
                        break;
                    }
                }
            }
        }
        ll ran=0;
        Rep(i,3){
            Rep(j,3){
                if(mat[i][j]){ran++ ; break;}
            }
        }
        cout<<ran<<endl;
    }
}