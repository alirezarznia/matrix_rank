problem : 
https://practice.geeksforgeeks.org/problems/create-your-own-calculator/0


describe:
https://www.geeksforgeeks.org/program-for-gauss-jordan-elimination-method/





#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;
typedef long double ld;
using namespace std;
//
ld arr[3][4];

ll CO(){
    Rep(i ,3){
        if(!arr[i][i]){
            ll numm= -1;
            For(j ,i+1 , 3) if(arr[j][i]) {numm =j;break;}
            if(numm == -1) return 0;
            Rep(k ,4)
                swap(arr[i][k] , arr[numm][k]);
        }
        Rep(j , 3){
            if(i != j){
                ld aa = arr[j][i]/arr[i][i];
                Rep(k ,4) arr[j][k]-=(aa*arr[i][k]);
            }
        }
    }
    return 1;
}
int main(){
  //  Test;
    ll t; cin >> t;
    while(t--){
        Rep(i, 3){
            Rep(j ,4) cin>> arr[i][j];
        }
        ll ff= CO();
//        Rep(i ,3){
//            Rep(j ,4) cout<<arr[i][j]<<"  ";
//            cout<<endl;
//            cout<<endl;
//        }
        if(!ff){
            Rep(i ,3){
                ld sum =0;
                Rep(j ,3) sum+=arr[i][j];
                if(abs(sum-arr[i][3])< EPS) ff =1;
            }
            cout<<ff<<endl;
        }
        else{
            Rep(i ,3) printf("%ld " ,(ll)floor(arr[i][3]/arr[i][i] ));
            cout<<endl;
        }
    }
    return 0;
}
When does this result in infinitely many solutions? No solutions?

    No solution: After applying row operations to make the lines parallel, one of the resulting equations will state that zero is equal to a nonzero number. This is a contradiction, and so the system has no solutions.
    Infinitely many solutions: After applying row operations to make the lines parallel, one of the resulting equations will become 0=0

. 